local lsp = require("lsp-zero")
local lspconfig = require("lspconfig")
require("lspconfig.ui.windows").default_options.border = "single"
local lsp_capabilities = require("cmp_nvim_lsp").default_capabilities()

require("mason").setup({ ui = { border = "rounded" } })
require("mason-lspconfig").setup({
	ensure_installed = {},
	handlers = { lsp.default_setup },
})

lspconfig.lua_ls.setup({
	capabilities = lsp_capabilities,
	settings = {
		Lua = {
			runtime = { version = "LuaJIT" },
			diagnostics = { globals = { "vim" } },
			workspace = { library = vim.api.nvim_get_runtime_file("", true) },
		},
	},
})

lspconfig.pyright.setup({
	capabilities = lsp_capabilities,
	on_attach = function(client, bufnr)
		client.server_capabilities.completionProvider = false
	end,
	settings = { python = { disableLanguageServices = true } },
})

lspconfig.jedi_language_server.setup({ capabilities = lsp_capabilities })

lspconfig.tsserver.setup({
	capabilities = lsp_capabilities,
	commands = {
		OrganizeImports = {
			function()
				local params = {
					command = "_typescript.organizeImports",
					arguments = { vim.api.nvim_buf_get_name(0) },
					title = "",
				}
				vim.lsp.buf.execute_command(params)
			end,
			description = "Organize Imports",
		},
	},
})

lsp.set_sign_icons({ error = "E", warn = "W", hint = "H", info = "I" })
vim.diagnostic.config({
	virtual_text = true,
	signs = true,
	update_in_instert = false,
	underline = false,
	severity_sort = true,
	float = { border = "rounded" },
})

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = "rounded" })

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
	border = "rounded",
	close_events = { "CursorMoved", "BufHidden" },
})

local cmp = require("cmp")
local cmp_select = { behavior = cmp.SelectBehavior.Select }

local luasnip = require("luasnip")

cmp.setup({
	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end,
	},
	sources = {
		{ name = "nvim_lsp" },
		{ name = "luasnip" },
		{ name = "nvim_lua" },
		{ name = "path" },
	},
	window = {
		completion = {
			border = "rounded",
			winhighlight = "Normal:NormalFloat,FloatBorder:NormalFloat",
		},
		documentation = {
			border = "rounded",
			-- winhighlight = "Normal:Pmenu,FloatBorder:Pmenu,Search:None"
		},
	},
	mapping = cmp.mapping.preset.insert({
		["<C-Space>"] = cmp.mapping.complete(),
		["<CR>"] = cmp.mapping.confirm({ select = false }),
		["<Tab>"] = cmp.mapping.select_next_item(cmp_select),
		["<S-Tab>"] = cmp.mapping.select_prev_item(cmp_select),
	}),
})

local null_ls = require("null-ls")
local group = vim.api.nvim_create_augroup("lsp_format_on_save", { clear = false })
local event = "BufWritePre" -- or "BufWritePost"
local async = event == "BufWritePost"

null_ls.setup({
	sources = {
		null_ls.builtins.formatting.prettier,
		null_ls.builtins.formatting.rustfmt,
		null_ls.builtins.formatting.stylua,
		null_ls.builtins.formatting.black,
		null_ls.builtins.formatting.markdownlint,
	},
	on_attach = function(client, bufnr)
		if client.supports_method("textDocument/formatting") then
			-- vim.keymap.set("n", "<Leader>f", function()
			--     vim.lsp.buf.format({ bufnr = vim.api.nvim_get_current_buf() })
			-- end, { buffer = bufnr, desc = "[lsp] format" })

			-- format on save
			vim.api.nvim_clear_autocmds({ buffer = bufnr, group = group })
			vim.api.nvim_create_autocmd(event, {
				buffer = bufnr,
				group = group,
				callback = function()
					vim.lsp.buf.format({ bufnr = bufnr, async = async })
				end,
				desc = "[lsp] format on save",
			})
		end

		if client.supports_method("textDocument/rangeFormatting") then
			-- vim.keymap.set("x", "<Leader>f", function()
			--     vim.lsp.buf.format({ bufnr = vim.api.nvim_get_current_buf() })
			-- end, { buffer = bufnr, desc = "[lsp] format" })

			-- format on save
			vim.api.nvim_clear_autocmds({ buffer = bufnr, group = group })
			vim.api.nvim_create_autocmd(event, {
				buffer = bufnr,
				group = group,
				callback = function()
					vim.lsp.buf.format({ bufnr = bufnr, async = async })
				end,
				desc = "[lsp] format on save",
			})
		end
	end,
})
