require("telescope").setup({
    extensions = {
        file_browser = {
            hijack_netrw = true,
            initial_mode = "normal",
            mappings = {["i"] = {}, ["n"] = {}}
        },
        fzf = {
            fuzzy = true,
            override_generic_sorter = true,
            override_file_sorter = true,
            case_mode = "smart_case"
        }
    },
    defaults = {file_ignore_patterns = {"%.git", "node_module", "main.js"}},
    pickers = {buffers = {initial_mode = "normal"}}
})
require("telescope").load_extension("fzf")
require("telescope").load_extension("file_browser")
