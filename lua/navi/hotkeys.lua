vim.g.mapleader = " "
vim.keymap.set("n", "<C-s>", ":update<CR>", { silent = true })
vim.keymap.set("n", "<leader>d", ":Telescope file_browser<CR>",
    { silent = true, noremap = true })

vim.keymap.set("n", "<leader>vs", ":FocusSplitNicely<CR>")

vim.keymap.set("n", "<leader>b", function()
    return require("telescope.builtin").buffers(
        require("telescope.themes").get_ivy({}))
end)
vim.keymap.set("n", "ff", function()
    return require("telescope.builtin").find_files(
        require("telescope.themes").get_ivy({}))
end)
vim.keymap.set("n", "fs", function()
    return require("telescope.builtin").live_grep(
        require("telescope.themes").get_ivy({}))
end)

vim.keymap.set("n", "<leader>so", ":SymbolsOutline<CR>")
vim.keymap.set("n", "dn", function() return vim.diagnostic.goto_next() end)
vim.keymap.set("n", "dp", function() return vim.diagnostic.goto_prev() end)
vim.keymap.set("n", "<leader>e",
    function() return vim.diagnostic.open_float() end)

vim.keymap.set("n", "<leader>ca",
    function() return vim.lsp.buf.code_action() end, { silent = true })

vim.keymap.set("n", "<leader>xx", function() require("trouble").toggle() end)
vim.keymap.set("n", "<leader>xw", function()
    require("trouble").toggle("workspace_diagnostics")
end)
vim.keymap.set("n", "<leader>xd",
    function() require("trouble").toggle("document_diagnostics") end)
vim.keymap.set("n", "<leader>xq",
    function() require("trouble").toggle("quickfix") end)
vim.keymap.set("n", "<leader>xl",
    function() require("trouble").toggle("loclist") end)
vim.keymap.set("n", "gR",
    function() require("trouble").toggle("lsp_references") end)

vim.keymap.set('n', 'K', function() vim.lsp.buf.hover() end)
vim.keymap.set('n', 'gd', function() vim.lsp.buf.definition() end)
vim.keymap.set('n', 'gD', function() vim.lsp.buf.declaration() end)
vim.keymap.set('n', 'gi', function() vim.lsp.buf.implementation() end)
vim.keymap.set('n', 'go', function() vim.lsp.buf.type_definition() end)
vim.keymap.set('n', 'gr', function() vim.lsp.buf.references() end)
vim.keymap.set('n', 'gs', function() vim.lsp.buf.signature_help() end)
vim.keymap.set('n', '<F2>', function() vim.lsp.buf.rename() end)
vim.keymap.set('n', '<c-f>', function() vim.lsp.buf.format({ async = true }) end)
vim.keymap.set('i', '<c-s>', vim.lsp.buf.signature_help)
