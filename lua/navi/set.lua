vim.opt.guicursor = ""
vim.opt.mouse = "a"

vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.o.showtabline = 1
vim.opt.laststatus = 3
vim.opt.cursorline = false

vim.cmd([[:set hidden]])
vim.cmd([[:set clipboard+=unnamedplus]])
vim.cmd([[let g:vim_matchtag_enable_by_default = 1]])

vim.g.skip_ts_context_commentstring_module = true

local ignore_filetypes = {
    'TelescopePrompt', 'toggleterm', 'help', 'Trouble', 'Outline'
}
local ignore_buftypes = {
    'nofile', 'prompt', 'popup', 'toggleterm', 'Trouble', 'Outline'
}

local augroup = vim.api.nvim_create_augroup('FocusDisable', {clear = true})
vim.api.nvim_create_autocmd('WinEnter', {
    group = augroup,
    callback = function(_)
        if vim.tbl_contains(ignore_buftypes, vim.bo.buftype) then
            vim.w.focus_disable = true
        else
            vim.w.focus_disable = false
        end
    end,
    desc = 'Disable focus autoresize for BufType'
})

vim.api.nvim_create_autocmd('FileType', {
    group = augroup,
    callback = function(_)
        if vim.tbl_contains(ignore_filetypes, vim.bo.filetype) then
            vim.w.focus_disable = true
        else
            vim.w.focus_disable = false
        end
    end,
    desc = 'Disable focus autoresize for FileType'
})
